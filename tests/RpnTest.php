<?php

/**
 * Unit tests for the Rpn Calculator
 *
 * @author mchristian
 */

include __DIR__ . "/../src/Rpn.php";

class RpnTest extends PHPUnit_Framework_TestCase {

    private $rpn; // placeholder for the Rpn

    function __construct() {
        $this->rpn = new Rpn(); // always need it, so go ahead and instantiate it
    }

    /**
     * @covers Rpn::getVerbose
     * @covers Rpn::setVerbose
     */
    public function testGetSetVerbose() {
        // true
        $this->rpn->setVerbose(true);
        // Assert
        $this->assertEquals(true, $this->rpn->getVerbose());

        // false
        $this->rpn->setVerbose(false);
        // Assert
        $this->assertEquals(false, $this->rpn->getVerbose());
    }

 
    /**
     * @covers Rpn::isVerbose
     */
    public function testIsVerbose() {

        // happy path
        $args = array("filename", "-v"); // expecting -v in the 2nd position
        $this->rpn->isVerbose($args);
        // Assert
        $this->assertEquals(true, $this->rpn->getVerbose());

        // empty
        $this->rpn->isVerbose(array());
        $this->assertEquals(false, $this->rpn->getVerbose());

        // garbage
        $this->rpn->isVerbose(array("f", "sdf", "198sdkfjhsdf"));
        $this->assertEquals(false, $this->rpn->getVerbose());
    }
    
    /**
     * @covers Rpn::isValidOperator
     */
    public function testIsValidOperator() {

        // success cases walk through all of the operators
        foreach (Rpn::$OPERATORS_BASIC as $operator) {
            $this->assertEquals(true, $this->rpn->isValidOperator($operator));
        }

        foreach (Rpn::$OPERATORS_SINGLE_VALUE as $operator) {
            $this->assertEquals(true, $this->rpn->isValidOperator($operator));
        }

        // failure case with some garbage
        foreach (array("sdafdsa", "918237h1!") as $operator) {
            $this->assertEquals(false, $this->rpn->isValidOperator($operator));
        }
    }
    
    /**
     * @covers Rpn::clearStack
     */
    public function testClearStack() {

        // put some stuff on the stack
        $this->rpn->calc = array(4, 34, 102, 10, "foo", "bar", "q3487");
        // make sure something is in there
        $this->assertEquals(true, in_array("foo", $this->rpn->calc));

        // now clear it out
        $this->rpn->clearStack();
        // make sure it's empty
        $this->assertEmpty($this->rpn->calc);
    }
    
    /**
     * @covers Rpn::getHelp
     */
    public function testGetHelp() {

        // lets see if we get something back...
        $this->assertNotEmpty($this->rpn->getHelp());
    }

    /**
     * @covers Rpn::process
     * @covers Rpn::calculate
     */
    public function testCalculate() {
        /*
         * Test all operators and make they work like we want.
         */

        // addition
        $this->rpn->process("5");
        $this->rpn->process("6");
        $this->rpn->process("7");
        $result = $this->rpn->process("+");
        $this->assertEquals(18, $result);
        $this->rpn->clearStack();
        
        // subtraction
        $this->rpn->process("10");
        $this->rpn->process("7");
        $result = $this->rpn->process("-");
        $this->assertEquals(3, $result);
        $this->rpn->clearStack();

        // multiplication
        $this->rpn->process("10");
        $this->rpn->process("7");
        $result = $this->rpn->process("*");
        $this->assertEquals(70, $result);
        $this->rpn->clearStack();

        // Division
        $this->rpn->process("18");
        $this->rpn->process("3");
        $result = $this->rpn->process("/");
        $this->assertEquals(6, $result);
        $this->rpn->clearStack();

        // Sine
        $this->rpn->process("60");
        $result = $this->rpn->process("sin");
        $this->assertEquals(-0.30481062110221668, $result);
        $this->rpn->clearStack();
    }

}
