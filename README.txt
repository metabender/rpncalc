Simple CLI RPN Calculator

Launch the application with an option (-v) flag to increase debug a little:
php rpn_cli.php 

Inline help is available once the application is launched by entering the
letter 'h' or the word 'help'.

Run the unit tests by running
./phpunit tests






