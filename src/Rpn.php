<?php

/**
 * Simple RPN Calculator
 * 
 * @author mchristian
 */

class Rpn {

    private $verbose = false; // flag to determine how much debug to show
    public $calc = array(); // array to hold our values

    /*
     * in PHP 5.6 and above these could be constants. Leaving as variables
     * so we don't have to deal with handling older verions.
     */
    public static $OPERATORS_BASIC = array("*", "/", "-", "+"); // basic multi value operators
    public static $OPERATORS_SINGLE_VALUE = array("sin"); // single value operators, cos maybe

    /**
     * Main method for operating the RPN calc
     *
     * Receives values passed in and determines what to do with them. If it's a number
     * it gets added to the stack, if it's a valid operator we kick off calculation.
     * Only valid values are numeric, or values found in the following arrays:
     * $OPERATORS_BASIC
     * $OPERATORS_SINGLE_VALUE
     *
     * @param string $stringToProcess 
     *
     * @return string boolean
     */

    public function process($stringToProcess) {

        // break apart the string into an array
        $token = explode(" ", $stringToProcess);
        $count = count($token);

        for ($i = 0; $i < $count; $i++) {

            echo $token[$i] . " \n"; // eecho the token back to the user

            if (is_numeric($token[$i])) {

                if ($this->getVerbose()) {
                    echo "Pushing " . $token[$i] . " onto the stack\n";
                }
                array_push($this->calc, $token[$i]);
                
            } else if (!is_numeric($token[$i])) {

                $operator = strtolower($token[$i]); // friendlier var name
                // figure what type of operator we are working with
                $operandType = $this->isvalidOperator($operator);

                if (!$operandType) {
                    // if it's not a number and not a valid operand let the user know
                    unset($token[$i]); // remove it from the array
                    return "INVALID"; // break out of the function and keep going.
                }

                if ($this->getVerbose()) {
                    echo "Calculating " . implode(" ", $this->calc) . " $operator \n";
                }

                // go ahead and run the calcultion
                $result = $this->calculate($operator);

                if ($result) {
                    // we've got a valid result, let's share it!
                    return $result;
                } else {
                    // something went wrong and we couldn't get a valid result.
                    // TODO: THIS is ambiguous.. and needs to have a better message
                    return "ERROR";
                }
            }
        }
    }

    /**
     * Method to handle calculation and stack manipulation
     *
     * This method is called by the process method once we receive a valid
     * operator. Once in here we determine what we need to do with the 
     * stack of numbers we should already have then pick a course of action
     * based on the operator specified.
     *
     * @param string $operator 
     *
     * @return boolean numeric
     */
    private function calculate($operator) {

        $result = false; // default result value
        
       
        for ($i = 0; $i < sizeof($this->calc); $i++) {
            // check to see if we need to do something with the stack for basic calc
            if (in_array($operator, self::$OPERATORS_BASIC)) {
                // move stuff around on the stack
                $secondOperand = end($this->calc);
                array_pop($this->calc);
                $firstOperand = end($this->calc);
                array_pop($this->calc);
            }

            // we can't use a variable as an soperator sooo... let's SWITCH it up... get it.
            switch ($operator) {
                case "*":
                    array_push($this->calc, $firstOperand * $secondOperand);
                    break;
                case "/":
                    array_push($this->calc, $firstOperand / $secondOperand);
                    break;
                case "+":
                    array_push($this->calc, $firstOperand + $secondOperand);
                    break;
                case "-":
                    array_push($this->calc, $firstOperand - $secondOperand);
                    break;
                case "sin":
                    $result = sin(end($this->calc));
                    array_push($this->calc, $result);
                    return $result;
                default:
                    /*
                     *  We've hit an operator we don't know how to handle.
                     *  Should never get here.. but if we do let's just return false.
                     */
                    return false;
            }
        }

        // return the result if we haven't gotten here.
        $result = end($this->calc);

        return $result;
    }

    /**
     * Determine the level of verbosity 
     *
     * Helper method to determine if the user has specified the verbose argument
     * on application launch. We use this to determine if want to print out 
     * a little extra information.
     *
     * @param array $args
     *
     * @return boolean numeric
     */
    public function isVerbose(array $args) {

        if (isset($args[1]) && $args[1] == "-v") {
            // get verbose!
            $this->setVerbose(true);
            return TRUE;
        } else {
            $this->setVerbose(false);
            return FALSE;
        }
    }

    /**
     * Retrieve some help info for the user
     *
     * Reads a basic help text file and returns it via STDOUT
     *
     * @return string boolean
     */
    public function getHelp() {

        /*
         * probably some security implications with just throwing a file out the stdout
         * lets live on the edge a little! 
         */

        // get contents of a file into a string
        $filename = "help.txt";
        $handle = fopen($filename, "r");
        if ($handle) {
            $contents = fread($handle, filesize($filename));
            fclose($handle);
            return $contents;
        } else {
            // bummer the help file isn't where we thought it was
            return false;
        }
    }
    
    /**
     * Remove all the values from the stack
     *
     * Clear the stack to start the calculation over. 
     *
     * @return boolean
     */
    public function clearStack() {
        // reset the array
        $this->calc = array();
    }
    
    /**
     * Determines if the operator being sent over is valid or not
     *
     * The available operators are held in class variables. We use this method
     * to make sure the value being passed in is indeed valid.
     *
     * @param string $operator
     * 
     * @return boolean
     */
    public function isValidOperator($operator) {

        if (in_array($operator, self::$OPERATORS_BASIC)) {
            return true;
        }

        if (in_array($operator, self::$OPERATORS_SINGLE_VALUE)) {
            return true;
        }

        // if we get here it's an invalid operator.
        return false;
    }

    /**
     * Set the value of the variable $verbose
     *
     * @param boolean
     */
    public function setVerbose($val) {
        $this->verbose = $val;
    }
    
     /**
     * Get the value of the variable $verbose
     *
     * @return boolean
     */
    public function getVerbose() {
        return $this->verbose;
    }

}
