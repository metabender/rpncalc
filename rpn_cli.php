<?php

include_once( __DIR__ . "/src/Rpn.php");

$rpn = new Rpn(); // new instance of RPN. We will always need it.
$verbose = false; // default verbosity
// 
echo "Usage help is available by type the letter 'h' or the word 'help'.\n\n";

// If we have some arguments, handle them.
if ($argv) {

    // lets see how chatty we should be.
    $rpn->isVerbose($argv);

    // show a message confirming if are in verbose mode
    if ($rpn->getVerbose()) {
        print("\nRunning in VERBOSE mode\n");
    }
}

// Start listening!
$stdin = fopen('php://stdin', 'r');
echo("> ");

while ($line = fgets($stdin)) {

    $line = strtolower(trim($line)); // get rid of whitespace.

    switch ($line) {
        // handle a help command
        case "help":
        case "h":
            $help = $rpn->getHelp();
            if ($help) {
                echo $help . "\n\n";
            } else {
                echo "\nSorry, the help file seems to be unavailable.\n";
            }
            break;
        // handle a quit command
        case "q":
        case "":
            print "\nThanks! Bye for now. \n";
            exit(0);
        // handle a clear command
        case "c":
            print "\nClearing... Ready to start over.\n";
            $rpn->clearStack();
            break;
        default:
            // let's process the line
            $result = $rpn->process($line);

            // get some results out for the user
            switch ($result) {
                case "ERROR":
                    echo "\n Sorry, something went wrong with the calculation.\n";
                    break;
                case "INVALID":
                    echo "The value you entered is not valid. Ignoring it.\n\n";
                    break;
                case true:
                    echo "\n##### Result: $result \n\n";
                default:
            }
    }

    echo("> ");
}










